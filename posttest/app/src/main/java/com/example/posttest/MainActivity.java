package com.example.posttest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Write a message to the database
//        FirebaseDatabase database = FirebaseDatabase.getInstance();
//        DatabaseReference myRef = database.getReference("message");
//
//        myRef.setValue("Hello, World!");

        mDatabase = FirebaseDatabase.getInstance().getReference();

        writeNewUser("1","test" , "test");

    }

    private void writeNewUser(String userId, String name, String email) {
        User user = new User(name, email);

        String key = mDatabase.child("users").push().getKey();

        mDatabase.child("users").child(key).setValue(user);
    }
}
