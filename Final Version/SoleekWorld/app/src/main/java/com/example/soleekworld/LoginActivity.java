package com.example.soleekworld;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {

    private EditText emailEditText;
    private EditText passwordEdiText;

    private String email;
    private String password;

    private Button loginButton;
    private Button registerButton;

    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Bint EditTexts
        emailEditText =  findViewById(R.id.loginActivityEmailEditText);
        passwordEdiText = findViewById(R.id.loginActivityPasswordEditText);

        // Bind Buttons
        loginButton = findViewById(R.id.loginActivityLoginButton);
        registerButton = (Button) findViewById(R.id.loginActivityRegisterButton);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginUser();
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });
    }

    private void loginUser(){
        if(CheckDataValidity()) {

            // Get User Data
            email = emailEditText.getText().toString().trim();
            password = passwordEdiText.getText().toString().trim();

            //Get Firebase auth instance
            auth = FirebaseAuth.getInstance();

            //authenticate user
            auth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            // If sign in fails, display a message to the user. If sign in succeeds
                            // the auth state listener will be notified and logic to handle the
                            // signed in user can be handled in the listener.
                            if (!task.isSuccessful()) {
                                // there was an error
                                if (password.length() < 6) {
                                    passwordEdiText.setError(getString(R.string.login_activity_password_lenght_error));
                                } else {
                                    Toast.makeText(LoginActivity.this, getString(R.string.login_activity_login_failed_error), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                startActivity(new Intent(LoginActivity.this, CountriesActivity.class));
                                finish();
                            }
                        }
                    });
        }
    }

    private boolean CheckDataValidity(){
        if (emailEditText.getText().toString().trim().isEmpty()) {
            emailEditText.setError(getString(R.string.login_activity_email_is_empty_error));
            return false;
        }

        if (passwordEdiText.getText().toString().trim().isEmpty()) {
            passwordEdiText.setError(getString(R.string.login_activity_password_is_empty_error));
            return false;
        }

        return true;
    }
}
