package com.example.soleekworld;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

// TODO : verify emails from Firebase UI Auth and manual
// TODO : Fix Gmail sign-in in Firebase Auth
// com.google.android.gms.common.api.ApiException: 12500 (code : 12500 , message : 12500)
// I have searched for this problem an all results say a problem in SH1
// althoug it is generated automatically from firebase assistant in android studio
// but I checked it with the value in firebase console and I found a difference in
// in capital letters so I modified it to found the new error
// com.google.android.gms.common.api.ApiException: 7 (code : 10 , message : 10)
// and after searching for it I didn't find anything else it is a misconfig and sync is
// required and on doing the old error appears again
// one stackoverflow link saied to add the SH1 to the google api console but I wasn't
// able to find the console but with trying in creditial it gives me a message that same
// SH1 is used in another app which I wasn't able to understand
//
// Second day I used to make sure of the SH1 in firebase console and Google API console
// are the same but this didn't solve the problem and after debuging my code I found the error
// GoogleSignInAccount account = task.getResult(ApiException.class);
// I tried to log the account and the task but the only thing that I noticed that the task owner
// is null so I searched for another way and found this link but I wasn't able to apply it
// https://github.com/Ginowine/android-firebase-authentication
// https://android.jlelse.eu/android-firebase-authentication-with-google-signin-3f878d9b7553

public class RegisterActivity extends AppCompatActivity {

    private int RC_SIGN_IN = 505;

    private EditText emailEditText;
    private EditText passwordEdiText;
    private EditText passwordConfirmEditText;

    private String email;
    private String password;

    private Button registerButton;
    private Button loginButton;
    private Button gmailButton;
    private Button manualButton;

    private FirebaseAuth auth;
    private DatabaseReference mDatabase;
    private CallbackManager mCallbackManager;
    private GoogleSignInClient googleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // Get refrece to database
        mDatabase = FirebaseDatabase.getInstance().getReference();

        // Bind EditTexts
        emailEditText =  findViewById(R.id.registerActivityEmailEditText);
        passwordEdiText = findViewById(R.id.registerActivityPasswordEditText);
        passwordConfirmEditText = findViewById(R.id.registerActivityPasswordConfirmEditText);

        // Bind Buttons
        registerButton = findViewById(R.id.registerActivityRegisterButton);
        loginButton = findViewById(R.id.registerActivityLoginButton);
        gmailButton = findViewById(R.id.registerActivityGmailButton);
        manualButton = findViewById(R.id.registerActivityManualRegisterButton);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firebaseAuthRegisterUser();
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
            }
        });

        gmailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gmailRegisterUser();
            }
        });

        manualButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manualRegisterUser();
            }
        });

        setGmailOptions();
        facebookRegisterUser();
    }

    //-------------------------------------- Firebase Auth --------------------------------------//

    private void firebaseAuthRegisterUser(){
        if(CheckDataValidity()){

            // Get user data
            email = emailEditText.getText().toString().trim();
            password = passwordEdiText.getText().toString().trim();

            //Get Firebase auth instance
            auth = FirebaseAuth.getInstance();

            //create user
            auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            Toast.makeText(RegisterActivity.this, "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();
                            // If sign in fails, display a message to the user. If sign in succeeds
                            // the auth state listener will be notified and logic to handle the
                            // signed in user can be handled in the listener.
                            if (!task.isSuccessful()) {
                                Toast.makeText(RegisterActivity.this, getString(R.string.register_activity_register_failed_error)+ task.getException(),
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                startActivity(new Intent(RegisterActivity.this, CountriesActivity.class));
                                finish();
                            }
                        }
                    });

        }
    }

    private boolean CheckDataValidity(){
        if (emailEditText.getText().toString().trim().isEmpty()) {
            emailEditText.setError(getString(R.string.register_activity_email_is_empty_error));
            return false;
        }

        if (passwordEdiText.getText().toString().trim().isEmpty()) {
            passwordEdiText.setError(getString(R.string.register_activity_password_is_empty_error));
            return false;
        }

        if (passwordEdiText.getText().toString().trim().length() < 6) {
            passwordEdiText.setError(getString(R.string.register_activity_password_lenght_error));
            return false;
        }

        if (passwordConfirmEditText.getText().toString().trim().isEmpty()) {
            passwordConfirmEditText.setError(getString(R.string.register_activity_password_confirm_is_empty_error));
            return false;
        }

        if(!passwordEdiText.getText().toString().trim().equals(passwordConfirmEditText.getText().toString().trim())){
            passwordConfirmEditText.setError(getString(R.string.register_activity_password_not_identical));
            return false;
        }

        return true;
    }

    //------------------------------------- Manual ----------------------------------------------//

    private void manualRegisterUser(){
        if(CheckDataValidity()){

            // Get user data
            email = emailEditText.getText().toString().trim();
            password = passwordEdiText.getText().toString().trim();

            User user = new User(email, password);

            String key = mDatabase.child("users").push().getKey();

            mDatabase.child("users").child(key).setValue(user);

            startActivity(new Intent(RegisterActivity.this, CountriesActivity.class));
        }
    }

    //------------------------------------- Facebook --------------------------------------------//

    private void facebookRegisterUser(){

        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance();

        // Initialize Facebook Login button
        mCallbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = findViewById(R.id.registerActivityFacebookButton);
        loginButton.setReadPermissions("email", "public_profile");

        // Callback registration
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException exception) {
                Toast.makeText(RegisterActivity.this, getString(R.string.register_activity_register_failed_error),Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result back to the Facebook SDK
        mCallbackManager.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Toast.makeText(getApplicationContext(),getText(R.string.register_activity_gmail_register_failed_error),Toast.LENGTH_SHORT).show();
                Log.d("fix gmail" , e.toString());
            }
        }
    }

    private void handleFacebookAccessToken(AccessToken token) {
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            startActivity(new Intent(RegisterActivity.this, CountriesActivity.class));
                            finish();
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(RegisterActivity.this, getString(R.string.register_activity_register_failed_error),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    //------------------------------------------ Gmail -------------------------------------------//

    private void setGmailOptions(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        googleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void gmailRegisterUser(){
        Intent signInIntent = googleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);

        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance();

        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Toast.makeText(RegisterActivity.this, "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();

                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(getApplicationContext(),getText(R.string.register_activity_gmail_register_failed_error),Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }
}
