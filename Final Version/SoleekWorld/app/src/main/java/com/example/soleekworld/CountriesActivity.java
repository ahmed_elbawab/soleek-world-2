package com.example.soleekworld;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

// TODO : Use loader to save fetched api  data from being lost on phone rotate
// TODO : Use Retrofit to fetch api data
// TODO : Load data in offline mode from local

public class CountriesActivity extends AppCompatActivity {

    private ArrayList<String> countries = new ArrayList<>();

    private ListView listView;

    private String url = "https://restcountries.eu/rest/v2/all";

    private Button logout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_countries);

        // Bind List
        listView = findViewById(R.id.listview);

        // Bind Button
        logout = findViewById(R.id.countriesActivityLogoutButton);

        // Create an object from countries loader to handel HTTP request in background thread
        CountriesLoader countriesLoader = new CountriesLoader();

        try {
            countries = countriesLoader.execute(url).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Adapter for the list view
        ArrayAdapter adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, countries);

        listView.setAdapter(adapter);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(CountriesActivity.this, MainActivity.class));
                finish();
            }
        });
    }

}
