package com.example.soleek_world;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.content.AsyncTaskLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Loads a list of countries by using an AsyncTask to perform the
 * network request to the given URL.
 */
public class CountriesLoader extends AsyncTask<String , Void , ArrayList<String> > {

    @Override
    protected ArrayList<String> doInBackground(String... strings) {
        String stringUrl = strings[0];
        ArrayList<String> countries = QueryUtils.fetchEarthquakeData(stringUrl);
        return countries;
    }
}
