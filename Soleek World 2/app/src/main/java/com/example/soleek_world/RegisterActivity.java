package com.example.soleek_world;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Logger;

import java.util.Date;

public class RegisterActivity extends AppCompatActivity {

    private int RC_SIGN_IN = 505;

    private EditText emailEditText;
    private EditText passwordEdiText;
    private EditText passwordConfirmEditText;

    private String email;
    private String password;

    private Button registerButton;
    private Button loginButton;
    private Button gmailButton;
    private Button facebookButton;
    private Button manualButton;

    private FirebaseAuth auth;
    private GoogleSignInClient googleSignInClient;

    // TODO : Fix google sign in
    // com.google.android.gms.common.api.ApiException: 12500 (code : 12500 , message : 12500)
    // I have searched for this problem an all results say a problem in SH1
    // althoug it is generated automatically from firebase assistant in android studio
    // but I checked it with the value in firebase console and I found a difference in
    // in capital letters so I modified it to found the new error
    // com.google.android.gms.common.api.ApiException: 7 (code : 10 , message : 10)
    // and after searching for it I didn't find anything else it is a misconfig and sync is
    // required and on doing the old error appears again
    // one stackoverflow link saied to add the SH1 to the google api console but I wasn't
    // able to find the console but with trying in creditial it gives me a message that same
    // SH1 is used in another app which I wasn't able to understand

    // TODO : add facebook sign in
    // TODO : verify emails

    // TODO : fix push method
    // Uncaught exception in Firebase runloop (3.0.0)
    // https://stackoverflow.com/questions/37724801/uncaught-exception-in-firebase-runloop-3-0-0
    // After along search I wasn't able to find an exact solution for the push method problem
    // I found a stackoverflow discussion with says it is a problem in firebase
    // to solve it use dynamic endAt and startAt param
    // but they are using the app offline while it isn't the case with me
    // besides that they solve it for query while I need to write data
    // so solution which I can done is to use firebase auth or use cloud fire store

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        emailEditText =  findViewById(R.id.registerActivityEmailEditText);
        passwordEdiText = findViewById(R.id.registerActivityPasswordEditText);
        passwordConfirmEditText = findViewById(R.id.registerActivityPasswordConfirmEditText);

        registerButton = findViewById(R.id.registerActivityRegisterButton);
        loginButton = findViewById(R.id.registerActivityLoginButton);
        gmailButton = findViewById(R.id.registerActivityGmailButton);
        facebookButton = findViewById(R.id.registerActivityFacebookButton);
        manualButton = findViewById(R.id.registerActivityManualRegisterButton);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
            }
        });

        gmailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gmailRegisterUser();
            }
        });

        facebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                facebookRegisterUser();
            }
        });

        manualButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUserManually();
            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        googleSignInClient = GoogleSignIn.getClient(this, gso);

        // TODO : disabled until fix
        manualButton.setClickable(false);
    }

    private void registerUser(){
        if(CheckDataValidity()){

            email = emailEditText.getText().toString().trim();
            password = passwordEdiText.getText().toString().trim();

            //Get Firebase auth instance
            auth = FirebaseAuth.getInstance();

            //create user
            auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            Toast.makeText(RegisterActivity.this, "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();
                            // If sign in fails, display a message to the user. If sign in succeeds
                            // the auth state listener will be notified and logic to handle the
                            // signed in user can be handled in the listener.
                            if (!task.isSuccessful()) {
                                Toast.makeText(RegisterActivity.this, getString(R.string.register_activity_register_failed_error)+ task.getException(),
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                startActivity(new Intent(RegisterActivity.this, CountriesActivity.class));
                                finish();
                            }
                        }
                    });

        }
    }

    private boolean CheckDataValidity(){
        if (emailEditText.getText().toString().trim().isEmpty()) {
            emailEditText.setError(getString(R.string.register_activity_email_is_empty_error));
            return false;
        }

        if (passwordEdiText.getText().toString().trim().isEmpty()) {
            passwordEdiText.setError(getString(R.string.register_activity_password_is_empty_error));
            return false;
        }

        if (passwordEdiText.getText().toString().trim().length() < 6) {
            passwordEdiText.setError(getString(R.string.register_activity_password_lenght_error));
            return false;
        }

        if (passwordConfirmEditText.getText().toString().trim().isEmpty()) {
            passwordConfirmEditText.setError(getString(R.string.register_activity_password_confirm_is_empty_error));
            return false;
        }

        if(!passwordEdiText.getText().toString().trim().equals(passwordConfirmEditText.getText().toString().trim())){
            passwordConfirmEditText.setError(getString(R.string.register_activity_password_not_identical));
            return false;
        }

        return true;
    }

    private void gmailRegisterUser(){
        Intent signInIntent = googleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void facebookRegisterUser(){
        // TODO : register with facebook
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Toast.makeText(getApplicationContext(),getText(R.string.register_activity_gmail_register_failed_error),Toast.LENGTH_SHORT).show();
                Log.d("fix gmail" , e.toString());
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);

        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance();

        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Toast.makeText(RegisterActivity.this, "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();

                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(getApplicationContext(),getText(R.string.register_activity_gmail_register_failed_error),Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }

    private void registerUserManually(){
        if(CheckDataValidity()){

            email = emailEditText.getText().toString().trim();
            password = passwordEdiText.getText().toString().trim();

            FirebaseDatabase database = FirebaseDatabase.getInstance();

            DatabaseReference myRef = database.getReference();

            // Creating new user node, which returns the unique key value
            // new user node would be /users/$userid/
            String userId = myRef.child("users").push().getKey();

            // creating user object
            User user = new User(email , password , false);

            // pushing user to 'users' node using the userId
            myRef.child("users").child(userId).setValue(user);

            startActivity(new Intent(RegisterActivity.this, CountriesActivity.class));
            finish();
        }
    }
}
