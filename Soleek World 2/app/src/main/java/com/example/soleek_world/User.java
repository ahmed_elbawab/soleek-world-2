package com.example.soleek_world;

public class User {

    private String email;
    private String password;
    private boolean verified;

    public User(){

    }

    public User(String email, String password, boolean verified) {
        this.email = email;
        this.password = password;
        this.verified = verified;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }
}
